-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 24, 2019 at 05:41 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_test`
--

DROP TABLE IF EXISTS `tbl_test`;
CREATE TABLE IF NOT EXISTS `tbl_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `APIName` varchar(200) DEFAULT NULL,
  `method` varchar(10) NOT NULL DEFAULT 'GET',
  `url` varchar(200) NOT NULL,
  `params` json DEFAULT NULL,
  `expect` json DEFAULT NULL,
  `actual` json DEFAULT NULL,
  `statuscode` int(11) DEFAULT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_test`
--

INSERT INTO `tbl_test` (`id`, `APIName`, `method`, `url`, `params`, `expect`, `actual`, `statuscode`, `time`, `status`) VALUES
(1, 'Tổng', 'GET', 'http://localhost:49898/api/tinh', '[\"so1=5\", \"so2=4\"]', '[\"so1\"]', '', , , 0),
(2, 'Tổng', 'GET', 'http://localhost:49898/api/tinh', '[\"so1=5\", \"so2=4\"]', '[9]', '', , , 0),
(3, 'Hiệu', 'GET', 'http://localhost:49898/api/tinh', '[\"so1=5\", \"so2=4\", \"so3=5\"]', '', '', , , 0),
(4, 'Chuỗi', 'GET', 'http://localhost:49898/api/tinh', NULL, '[\"hello\"]', '[]', , , 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
