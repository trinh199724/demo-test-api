package Domain;
import java.util.List;

public class TestResult {
	public TestResult(String url, List<String> Params, String Expect) {
		this.endpoint = url;
		this.params = Params;
		this.Expect = Expect;
	}
	public TestResult(int id,String method,String url, List<String> Params, String Expect) {
		this.endpoint = url;
		this.params = Params;
		this.id = id;
		this.Expect = Expect;
		this.Method = method.toUpperCase();
	}
	public int id;
	public String endpoint;
	public List<String> params;
	public String Actual;
	public String Expect;
	public int APIStatusCode;
	public Boolean Status;
	public String Method;
	public long Time;
}