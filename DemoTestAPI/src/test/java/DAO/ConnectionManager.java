package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {

	//final static String DRIVER = ResourceBundle.getBundle("db").getString("driver");
	static Cons con;

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //System.out.println("Driver loaded.");
            connection = DriverManager.getConnection(con.SERVER, con.USERNAME, con.PASSWORD);
            //System.out.println("Connection established.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(Connection... connections) {
        if (connections == null) {
            return;
        }
        for (Connection connection : connections) {
            if (connection != null) {
                try {
                    connection.close();
                    System.out.println("Connection closed.");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void close(Statement... statements) {
        if (statements == null) {
            return;
        }
        for (Statement statement : statements) {
            if (statement != null) {
                try {
                    statement.close();
                    System.out.println("Statement closed.");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void close(ResultSet... resultSets) {
        if (resultSets == null) {
            return;
        }
        for (ResultSet resultSet : resultSets) {
            if (resultSet != null) {
                try {
                    resultSet.close();
                    System.out.println("ResultSet closed.");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
