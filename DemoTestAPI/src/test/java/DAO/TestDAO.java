package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Domain.TestResult;

public class TestDAO {
	private final Connection con;
	static ArrayList<String> lstParamStr = null;

	public TestDAO() {
		con = ConnectionManager.getConnection();
	}

	//đọc dữ liệu từ DB
	public ArrayList<TestResult> getAll() throws SQLException {
		ArrayList<TestResult> lst = new ArrayList<>();
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.getConnection();
			String query = "Select * From tbl_test";
			pstm = con.prepareStatement(query);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt(1);
				String method = rs.getString(3);
				String url = rs.getString(4);
				
				ArrayList<String> param=new ArrayList<>();
				if(rs.getString(5)==null)
				{
					param=null;
				}
				
				else param = convertParams(rs.getString(5));
				
				String expect = "";
				if (rs.getString(6) != null) {
					String str = rs.getString(6);
					expect = converJSontoString(str);
				}
				TestResult ls = new TestResult(id,method, url,param,expect);
				lst.add(ls);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (pstm != null) {
				pstm.close();
			}
			if (con != null) {
				con.close();
			}
		}
		return lst;
	}


	private static ArrayList<String> convertParams(String param) {
		if (!param.isEmpty() || param == null) {
			String str = param;
			if (!str.trim().isEmpty()) {
				if (countequal(param) >= 1) {
					return lstParamStr;
				}
			}
		}
		return null;
	}

	//Chuyển đổi kiểu dữ liệu Json sang chuỗi String
	private static String converJSontoString(String param) {
		if (!param.isEmpty()) {
			String str = param;
			if (!str.trim().isEmpty()) {
				if (countequal(param) >= 1) {
					str = "";
					for (String string : lstParamStr) {
						str += string;
					}
					return str;
				}
			}
		}
		return null;
	}

	// đếm tham số(param)
	private static int countequal(String param) {
		/*
		 * char ch = "=".charAt(0); int count = 0; for (int i = 0; i <
		 * param.length(); i++) { if (ch == param.charAt(i)) { count++; } }
		 * return count;
		 */
		int count = 0;
		if (convertJsontoString(param).size() > 0) {
			count = convertJsontoString(param).size();
		}
		return count;

	}

	//Chuyển từ  Json sang mảng String
	private static ArrayList<String> convertJsontoString(String jsonStr) {
		lstParamStr = new ArrayList<>();
		try {
			JSONParser jsonParser = new JSONParser();

			// Parser chuỗi JSON về một JSONArray
			JSONArray jsonArray = (JSONArray) jsonParser.parse(jsonStr);

			// Lấy các giá trị trong jsonArray bằng cách lặp qa từng phần tử
			for (int i = 0; i < jsonArray.size(); i++) {

				Object ob = jsonArray.get(i);
				if (ob != null)
					lstParamStr.add(ob.toString());
			}
		} catch (ParseException e) {
			System.out.print(e.getMessage());
		} finally {
			return lstParamStr;
		}
	}

	//Chuyển từ String sang Json
	private static String convertStringtoJSon(String string) {
		String str = "";

		if (string != null || !string.isEmpty()) {
			str = "[" + string + "]";
			return str;
		}
		return null;
	}

	//Cập nhật/ghi dữ liệu vào db
	public int updateTestResult(TestResult trs) throws SQLException {
		int affectedRows = -1;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = ConnectionManager.getConnection();
			String query = "Update tbl_test Set actual=?, statuscode=?, status=?,time=? where id=?";
			pstm = con.prepareStatement(query);

			pstm.setString(1, convertStringtoJSon(trs.Actual));
			pstm.setInt(2, trs.APIStatusCode);
			pstm.setBoolean(3, trs.Status);
			pstm.setInt(4, (int) trs.Time);

			pstm.setInt(5, trs.id);
			affectedRows = pstm.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (pstm != null) {
				pstm.close();
			}
			if (con != null) {
				con.close();
			}
		}
		return affectedRows;
	}
}
