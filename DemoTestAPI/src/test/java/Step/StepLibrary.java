package Step;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;

import DAO.TestDAO;
import Domain.TestResult;
import net.thucydides.core.annotations.Step;

public class StepLibrary {
	public static ArrayList<TestResult> lst = new ArrayList<>();
	static TestDAO testDAO = new TestDAO();
	String url = "";

	@Step("get all data")
	public static void ReadData() throws SQLException {
		lst = testDAO.getAll();
		if (lst.size() < 0) {
			Assert.assertFalse(lst.size() < 0);
		} else
			Assert.assertTrue(lst.size() > 0);
	}

	@Step("check api")
	public void checkapi() throws SQLException {
		for (TestResult trs : lst) {
			callAPI(trs);
		}
	}

	private void UrlParam(TestResult trs) {
		String str = trs.endpoint;
		if (trs.params!=null) {
			str = str + "?";

			if (trs.params.size() >= 1) {
				str = str + trs.params.get(0);
				if (trs.params.size() > 1)
					for (int i = 1; i < trs.params.size(); i++) {
						str = str + "&" + trs.params.get(i);
					}
			}
		}
		url = str;
	}

	@Step("Call API and Update DB")
	private void callAPI(TestResult trs) throws SQLException {
		long millis1=0,millis2=0;
		int res = 0;
		StringBuilder sb = new StringBuilder();

		try {
			UrlParam(trs);
			URL obj = new URL(url);
			millis1 = System.currentTimeMillis();
			HttpURLConnection rescon = (HttpURLConnection) obj.openConnection();
			rescon.setRequestMethod(trs.Method);
			rescon.setRequestProperty("Content-Type", "application/json");

			res = rescon.getResponseCode();

			if (res == 200) {
				InputStreamReader in = null;
				try {

					if (rescon != null)
						rescon.setReadTimeout(60 * 1000);
					if (rescon != null && rescon.getInputStream() != null) {
						in = new InputStreamReader(rescon.getInputStream(), Charset.defaultCharset());
						BufferedReader bufferedReader = new BufferedReader(in);
						if (bufferedReader != null) {
							int cp;
							while ((cp = bufferedReader.read()) != -1) {
								sb.append((char) cp);
							}
							bufferedReader.close();
						}
					}
					in.close();
					millis2 = System.currentTimeMillis();
					
					System.out.println(trs.Expect + "-" + sb.toString());
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			} else {
				System.out.println(res + "");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			trs.Time =millis2-millis1;
			trs.APIStatusCode = res;
			trs.Actual = sb.toString();
			if (trs.Actual.replace("\"", "").equals(trs.Expect.replace("\"", ""))) {
				trs.Status = true;
			} else {
				trs.Status = false;
			}
			if (testDAO.updateTestResult(trs) != -1) {
				System.out.println("Ghi Thành công!");
			} else {
				System.out.println("Ghi Thất bại");
			}
			if (!trs.Status) {
				System.out.println("Fail");
			} else
				System.out.println(trs.Status + "-" + true);
		}
	}


}
