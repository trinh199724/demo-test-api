import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;

import Step.StepLibrary;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

@RunWith (SerenityRunner.class)
public class TestRunner {
	
	@Steps
	StepLibrary test;

	@Test
	public void checkAPI() throws SQLException
	{
		test.ReadData();
		test.checkapi();
	}
	
}
